# My emacs config

This is my emacs configuration.

This version uses literal programming with org-babel.

It is heavely influenced by [Arjen Wiersma's configuration](https://gitlab.com/buildfunthings/emacs-config/)

Go ahead, read the entire configuration by opening `loader.org`.